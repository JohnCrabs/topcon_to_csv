import os
import csv


class ToconToCSV:
    def __init__(self):
        self.raw_data = []
        self.dict_data = {}
        self.csv_data = []
        self.csv_header = ['station_id', 'station_height', 'scope_to', 'scope_height',
                  'horizontal_angle', 'vertical_angle', 'slope_distance_meters',
                  'horizontal_distance_meters']

    def read(self, path: str):
        if os.path.exists(path):
            with open(path) as file:
                data = file.readlines()[0].split('_\'')
                data.remove('')
                for __data__ in data:
                    station = __data__.strip().split('_+')
                    station_name = 'S0'
                    for __entry__ in station:
                        if __entry__.__contains__('_(_)'):
                            station_name, height = __entry__.split('_(_)')
                            self.dict_data[station_name] = {'height': float(height)}
                        else:
                            values, prism_height = __entry__.split('_*_,')
                            prism_height = float(prism_height.replace('_', '')) / 1000

                            prism_name, measurements = values.split('_ ?+')
                            slope_distance, measurements = measurements.split('m')
                            slope_distance = float(slope_distance) / 1000
                            angles, measurements = measurements.split('g+')
                            v_angle, h_angle = angles.split('+')
                            v_angle = float(v_angle) / 10000
                            h_angle = float(h_angle) / 10000
                            h_distance, measurements = measurements.split('t')
                            h_distance = float(h_distance) / 1000

                            self.dict_data[station_name][prism_name] = {
                                'height': str(prism_height),
                                'h_angle_grad': str(h_angle),
                                'v_angle_grad': str(v_angle),
                                'slope_distance_meters': str(slope_distance),
                                'horizontal_distance': str(h_distance),
                            }
                file.close()

        else:
            print(f"ERROR: File of path <{path}> doesn't exist!")

    def to_csv(self):
        for __key__ in self.dict_data.keys():
            for __entry_key__ in self.dict_data[__key__].keys():
                if __entry_key__ == 'height':
                    pass
                else:
                    values = [__key__, self.dict_data[__key__]['height'], __entry_key__,
                              self.dict_data[__key__][__entry_key__]['height'],
                              self.dict_data[__key__][__entry_key__]['h_angle_grad'],
                              self.dict_data[__key__][__entry_key__]['v_angle_grad'],
                              self.dict_data[__key__][__entry_key__]['slope_distance_meters'],
                              self.dict_data[__key__][__entry_key__]['horizontal_distance']]
                    self.csv_data.append(values)

    def export_to(self, path: str):
        with open(path, 'w', newline='') as file:
            write = csv.writer(file)
            write.writerow(self.csv_header)
            write.writerows(self.csv_data)
            file.close()


if __name__ == '__main__':
    data_path = "./data/05_07_2020"
    out_data = './data/05_07_2020.csv'
    topcon = ToconToCSV()
    topcon.read(path=data_path)
    topcon.to_csv()
    topcon.export_to(path=out_data)
